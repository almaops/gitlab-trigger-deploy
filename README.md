gitlab-trigger-deploy
=========

Just a container with a script triggering Gitlab deployment pipelines.

Usually we store deployment pipelines in some "IaC" repo (Infrastructure as Code).

To use this container you need to specify a job using this image, specify some variables to configure the trigger script, and run `gitlab-trigger-deploy` in the `script` section of your job.

Mandatory variables are: `DEPLOYMENT_PROJECT_ID`, `DEPLOYMENT_PIPELINE_TRIGGER_TOKEN` and `DEPLOYMENT_PIPELINE_READ_API_TOKEN`

All the other variables (you can learn them from the `Examples` section below) are just to adjust the deployment procedure behaviour.

Examples
-----

Put something like this in the `.gitlab-ci.yml`:

    .job-template-deployment: &job-template-deployment
      stage: deploy
      image: registry.gitlab.com/almaops/gitlab-trigger-deploy:latest
      script: gitlab-trigger-deploy
      allow_failure: true
      dependencies: []
      when: manual
    
    stage:deploy:
      <<: *job-template-deployment
      environment:
        name: "stage"
        url: "https://stage.example.com/"
      variables:
        DEPLOY_ENV: "stage"


Also you can specify other variables in any place where Gitlab supports it:


        ### We recommend to set the following variables in the .gitlab-ci.yml (just examples)
        # will be passed as ENV variable to the deployment pipeline, default "", optional
        DEPLOY_ENV: "stage"
        
        ### We recommend to set the following variables in Project CI/CD settings (just examples)
        # will be passed as VARS variable to the deployment pipeline, default "", optional
        DEPLOY_VARS: "deploy_branch=<...> deploy_tag=<...>"
        # will be passed as TAGS variable to the deployment pipeline, default "", optional
        DEPLOY_TAGS: "my-ansible-tag"
        
        ### we recommend to set the following variables in Group CI/CD settings (just exmaples)
        # the IaC project ID, you can find it IaC -> Settings -> General -> Project ID, MANDATORY
        DEPLOYMENT_PROJECT_ID: "123"
        # the IaC project branch to trigger the deployment pipeline on, default "master", optional
        DEPLOYMENT_BRANCH: "master"
        # The IaC project's trigger token, generate it with IaC -> Settings -> CI/CD -> Pipeline triggers, MANDATORY
        DEPLOYMENT_PIPELINE_TRIGGER_TOKEN: "<...>"
        # The IaC project's read_api token, generate it with IaC -> Settings -> CI/CD -> Access Tokens, MANDATORY
        DEPLOYMENT_PIPELINE_READ_API_TOKEN: "<...>"
        
        ### additional variables you can use
        # set it only if the IaC is on some other gitlab, default "${CI_SERVER_URL}" i.e. the same gitlab, optional
        GITLAB_SERVER_URL: "https://gitlab.example.com"
        # if you set it to any non-empty string, the script will just trigger deployment and immediately exit
        # in this case you won't need DEPLOYMENT_PIPELINE_READ_API_TOKEN
        # default is "", optional
        DEPLOYMENT_JUST_TRIGGER_AND_QUIT: "yes-but-am-I-sure?"
        # a triggerer job will wait a deployment pipeline to finish in order to inherit its status
        # you can adjust the time to wait with this variable, default is "300" i.e. 5 minutes, optional
        DEPLOYMENT_PIPELINE_TIMEOUT_SECONDS: "300"
        # a triggerer job will check a deployment pipeline's status every N seconds
        # you can adjust the interval with this variable, default is "10" seconds, optional
        DEPLOYMENT_PIPELINE_CHECK_INTERVAL_SECONDS: "10"

